(function(){
    angular
        .module("ellery")
        .config(uiRouteConfig);
    
    uiRouteConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function uiRouteConfig($stateProvider, $urlRouterProvider){
        
        $urlRouterProvider.otherwise("/home");
        
        $stateProvider
            .state("home", {
                url: "/home",
                templateUrl: "views/home.html"
            })
            .state("shop", {
                url: "/shop",
                templateUrl: "views/shop.html"
            })
            .state("contact", {
                url: "/contact",
                templateUrl: "views/contact.html"
            })
            .state("cart", {
                url: "/cart",
                templateUrl: "views/cart.html"
            })
            .state("faq", {
                url: "/faq",
                templateUrl: "views/faq.html"
            })
            .state("terms", {
                url: "/terms",
                templateUrl: "views/terms.html"
            })
             .state("privacy", {
                url: "/privacy",
                templateUrl: "views/privacy.html"
            })
            .state("product_list", {
                url: "/admin_product_list",
                templateUrl: "views/admin_product_list.html",
                controller: "ListCtrl as ctrl"
            })
            .state("product_edit", {
                url: "/admin_product_edit",
                templateUrl: "views/admin_product_edit.html",
                controller: "EditCtrl as ctrl"
            });
            /*
            .state("editWithParams", { 
               url: "/edit/:dept_no",
               templateUrl: "app/edit/edit.html",
            
           })
           */
    }
})();